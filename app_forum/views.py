from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.contrib import messages

import json
from app_profile.models import Perusahaan
from .models import Forum
from .forms import Forum_Form

response = {"author": "Ahmad Hasan Siregar"}
MAX_FORUM_PER_PAGE = 3
DEFAULT_DATETIME_FORMAT = "%Y-%m-%dT%H:%MZ"

def index(request):
	# TODO-integration: check if a request is logged in
	if "perusahaan_id" not in request.session:
		# TODO-integration: redirect to login page
		# return HttpResponseRedirect(reverse('app-login:index'))
		request.session["perusahaan_id"] = 1

	perusahaan_id = int(request.session["perusahaan_id"])
	response["perusahaan"] = Perusahaan.objects.get(pk=perusahaan_id)
	response["forum_form"] = Forum_Form
	response.update(json.loads(get_forum_posts(request).content.decode("utf-8")))

	html = "app_forum/forum.html"

	print("response index => ", response)
	return render(request, html, response)

# one-indexed page
def get_forum_posts(request):
	if request.method != 'GET' or "perusahaan_id" not in request.session:
		return HttpResponseRedirect(reverse('forum:index'))

	data = {"forums": []}
	page = int(request.GET.get("page", 1))

	# return empty forums
	if page < 1:
		return JsonResponse(data)

	perusahaan_id = int(request.session["perusahaan_id"])
	offset = 3*(page-1)
	query_results = Forum.objects.filter(perusahaan=perusahaan_id)[offset : offset+MAX_FORUM_PER_PAGE]
	
	for forum in query_results:
		data["forums"].append({"id": forum.id, "perusahaan_id" : forum.perusahaan.id, \
			"perusahaan_nama" : forum.perusahaan.nama_perusahaan, "content" : forum.content, \
			"posted_at" : forum.posted_at.strftime(DEFAULT_DATETIME_FORMAT)})

	return JsonResponse(data)

def post_forum(request):
	if request.method == "POST" and "perusahaan_id" in request.session:
		form = Forum_Form(request.POST or None)
		if form.is_valid():
			content = request.POST["content"]
			perusahaan = Perusahaan.objects.get(pk=int(request.session["perusahaan_id"]))
			new_forum = Forum(perusahaan=perusahaan, content=content)
			new_forum.save()


	return HttpResponseRedirect(reverse("forum:index"))
