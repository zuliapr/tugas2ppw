from django.db import models
from app_profile.models import Perusahaan

class Forum(models.Model):
	# id as primary-key
	perusahaan = models.ForeignKey(Perusahaan, on_delete=models.CASCADE)
	content = models.CharField(max_length=1000)
	posted_at = models.DateTimeField(auto_now_add=True)
