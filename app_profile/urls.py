from .views import index, load_profile, refresh
from django.conf.urls import url
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^load-profile/$', load_profile, name='load-profile'),
    url(r'^refresh/(?P<id>[-\w]+)/$', refresh, name='refresh'),
]